# ITT Final project

This is the Final project for Mohammed H. Almajedi in IT technology at UCL.

## Project description

The main goal of this project is  using IoT technology to create a prototype that can improve the quality of the environment for the people who are working from home because of lockdown due to COVID-19.

## About the prototype

The prototype uses different sensor to send real time data to the raspberry pi, which can control different devices according to the data received from the sensors as well as displaying the data on an OLED display module and sending notifications to both smartphone and computers.

## Block diagram

<img src="Materials/blockd.png" width="1000">

## Fritzing circuit design 

<img src="Fritzing circuit design/circuit.png" width="1000">

## Actual picture  of the prototype

<img src="Materials/acircuit.jpg" width="1000">

## Features
- Air quality sensor
- Temperature and humidity sensor
- light sensor
- OLED display that shows current readings from the sensors
- Fan that turns on according to the room temperature
- Light bulb that turns on according to the brightness
- Smartphone notifications

# Auto Light control
### the following demonstration shows one of the prototype features and that is how covering the light sensor turns on the light bulb

<img src="Materials/alight.gif" width="600">

## Requirements
### Hardware
#### All the following hardware and components was used in this project, similar products could be used for the same results.
- Raspberry Pi 3 B+
  https://raspberrypi.dk/produkt/raspberry-pi-3-model-b-plus/
- MCU-811 CCS811 Air Quality Digital Sensor
  https://www.elecrow.com/cjmcu-811-ccs811-carbon-monoxide-co-vocs-air-quality-digital-gas-sensor.html
- Philips Hue bridge
  https://www.power.dk/smart-home/smart-indendoers-belysning/startsaet-og-bundles/philips-hue-bridge/p-252281/
- Philips Hue light bulb
  https://www.power.dk/smart-home/smart-indendoers-belysning/smarte-lyspaerer/philips-hue-e14-bt-w-55w-b39-led-paere/p-1012773/
- USB Fan 5v
  https://www.biltema.dk/kontor---teknik/computertilbehor/computerkabler/usb-kabler/usb-ventilator-2000042678
- DHT11 Temperature and humidity sensor
  https://minielektro.dk/dht11-temperatur-og-fugtsensor.html?gclid=Cj0KCQjwytOEBhD5ARIsANnRjVjO7u3_1cxuus_kCwZzlDg1dnZWVeT60fnE6LA5lN9HUPcZCBI_94caAsorEALw_wcB
- LDR - Light Dependent Resistor
  https://minielektro.dk/ldr-photoresistor.html?gclid=Cj0KCQjwytOEBhD5ARIsANnRjVhFOkYWDTA-Za74WWw5TSg4tXU0SKReMcr44IZ4EgnutIEvAxSEbIoaAl-tEALw_wcB
- Capacitor 1uf 50v 
  https://www.elextra.dk/p/1uf-50v-lodret-elektrolyt/H11777
- OLED Display Module 0.96 
  https://www.u-buy.dk/en/catalog/product/view/id/30013042/s/0-96-oled-display-module-fbhdzvv-128-x-64-pixel-iic-12864-oled-blue
- NPN Transistor BC337-40
  https://www.conradelektronik.dk/p/diotec-transistor-bjt-diskret-bc337-40-to-92-3-antal-kanaler-1-npn-140536
- x2 LEDs
  https://minielektro.dk/lite-on-5mm-rod-led-diffuseret-19mcd-36.html?gclid=Cj0KCQjwytOEBhD5ARIsANnRjViAz-QeU88qgK53nrP9nw0C5tjXigXHBxMg_5HX3547ptFivaFUlDEaAuFDEALw_wcB
- x2 330 ohm Resistors
  https://minielektro.dk/330-ohm-5-1-4w-modstand.html
- x1 10K ohm Resistors
  https://minielektro.dk/10k-ohm-5-1-4w-modstand.html
- Female to Male USB cable
  https://www.amazon.com/AmazonBasics-Extension-Cable-Male-Female/dp/B00NH11PEY
- Jumper cables
  https://minielektro.dk/han-hun-breadboard-ledninger-40-x-200mm.html
- Breadboard
  https://let-elektronik.dk/shop/1150-breadboards--hulprint/12615-breadboard---full-size-bare/	
  
### Software
- Python 3
https://www.python.org/downloads/
- Advanced IP Scanner
https://www.advanced-ip-scanner.com
- Fritzing (Optional: Circuit design)
https://fritzing.org/download/
- Draw io (Optional: block diagram)
https://app.diagrams.net
- Pycharm Python IDE (Optional)
https://www.jetbrains.com/pycharm/download/#section=windows

### Python libraries
#### First update the raspberry pi
- `sudo apt update`
- `sudo apt upgrade`
#### Then install the following libraries
- `pip3 install phue`
- `pip3 install Adafruit_Python_DHT`
- `pip3 install adafruit-circuitpython-ccs811`
- `pip3 install Adafruit-SSD1306`
- `pip3 install pushbullet.py==0.9.1`

## Sources
- https://www.raspberrypi-spy.co.uk/2018/04/i2c-oled-display-module-with-raspberry-pi/
- https://github.com/studioimaginaire/phue
- https://pimylifeup.com/raspberry-pi-light-sensor/
- https://learn.adafruit.com/adafruit-ccs811-air-quality-sensor?view=all#install-python-software-8-1
- https://medium.com/@romanzipp/raspberry-pi-temperature-controlled-fan-2aa0de72a564
- https://github.com/Azelphur/pyPushBullet
- https://www.raspberrypi-spy.co.uk/2017/09/dht11-temperature-and-humidity-sensor-raspberry-pi/





## By: Mohammed Hussein Almajedi
### moha4424@edu.ucl.dk

<img src="Materials/ucl.png" width="150">