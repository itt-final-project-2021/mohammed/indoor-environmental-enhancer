import sensor
import time
import Adafruit_GPIO.SPI as SPI
import board
import busio
import Adafruit_SSD1306
import subprocess
from PIL import Image
from PIL import ImageDraw
from PIL import ImageFont
from pushbullet.pushbullet import PushBullet

# OLED

RST = None

DC = 23
SPI_PORT = 0
SPI_DEVICE = 0

# 128x32 display with hardware I2C:
disp = Adafruit_SSD1306.SSD1306_128_32(rst=RST)

# Initialize library.
disp.begin()

# Clear display.
disp.clear()
disp.display()

# Create blank image for drawing.
width = disp.width
height = disp.height
image = Image.new('1', (width, height))

# Get drawing object to draw on image.
draw = ImageDraw.Draw(image)

# Draw a black filled box to clear the image.
draw.rectangle((0, 0, width, height), outline=0, fill=0)

# Draw some shapes.
padding = -2
top = padding
bottom = height - padding
# Move left to right keeping track of the current x position for drawing shapes.
x = 0

# Load default font.
font = ImageFont.load_default()

# Pushbullet notifications
apiKey = "o.xxxxxxxxxxxxxxxxxxxx"  # Defining your API from pushbullet.com
p = PushBullet(apiKey)
devices = p.getDevices()
pushed = False
#######################################

try:
    # Main loop
    while True:
        # hue
        print('Brightness level(Higher=Darker):', rc_time(lightsens), '\n')
        if rc_time(lightsens) < 8000:
            light_off()

        else:
            light_n()

        # DHT11
        humidity, temperature = Adafruit_DHT.read_retry(sensor, gpio)  # Read from DHT11
        if humidity is not None and temperature is not None:
            print('Temp={0:0.1f}*C  Humidity={1:0.1f}%\n'.format(temperature, humidity))
        else:
            print('Failed to get reading. Try again!')

        if temperature > 22:
            fan_on()

        else:
            fan_off()

        if temperature > 22 and not pushed:  # Send notification to pushbullet once
            p.pushNote(devices[0]["iden"], 'Too warm!', 'Fan ON')
            pushed = True
        else:
            pass
        '''
        if humidity > 60:
            GPIO.output(24, GPIO.HIGH)  # Turn on Yellow LED
            print('Too much Humidity! Please Ventilate the room!\n')
        else:
            GPIO.output(24, GPIO.LOW)  # Turn off Yellow LED
        '''
        if humidity > 60 and not pushed:  # Send notification to pushbullet once
            p.pushNote(devices[0]["iden"], 'Too much Humidity!', 'Please Ventilate the room!')
            pushed = True
        else:
            pass

        # Air Quality sensor
        print('CO2: {} PPM, TVOC: {} PPB\n\n\n'.format(ccs811.eco2, ccs811.tvoc))
        if ccs811.eco2 > 1000:
            GPIO.output(21, GPIO.HIGH)  # Turn on Red LED
            print('Please Ventilate the room!\n')
        else:
            GPIO.output(21, GPIO.LOW)  # Turn off Red LED
        if ccs811.eco2 > 1000 and not pushed:  # Send notification to pushbullet once
            p.pushNote(devices[0]["iden"], 'Too much eco2!', 'Please Ventilate the room!')
            pushed = True
        else:
            pass

        if ccs811.tvoc > 60:
            GPIO.output(21, GPIO.HIGH)  # Turn on Red LED
            print('Please Ventilate the room!\n')
        else:
            GPIO.output(21, GPIO.LOW)  # Turn off Red LED
        if ccs811.tvoc > 60 and not pushed:  # Send notification to pushbullet once
            p.pushNote(devices[0]["iden"], 'Alert!', 'Please Ventilate the room!')
            pushed = True
        else:
            pass

        # OLED display
        draw.rectangle((0, 0, width, height), outline=0, fill=0)  # Draw a black filled box to clear the image.

        draw.text((x, top), str(' Temp={0:0.1f}C,Hum={1:0.1f}%'.format(temperature, humidity)), font=font, fill=255)
        draw.text((x, top + 8), " Brightness: " + str(rc_time(lightsens)), font=font, fill=255)
        draw.text((x, top + 16), ' CO2: ' + str(ccs811.eco2) + ' PPM', font=font, fill=255)
        draw.text((x, top + 25), ' TVOC: ' + str(ccs811.tvoc) + ' PPB', font=font, fill=255)
        disp.image(image)
        disp.display()

        time.sleep(0.1)


except KeyboardInterrupt:
    pass
finally:
    GPIO.cleanup()
