import RPi.GPIO as GPIO
import Adafruit_DHT
import adafruit_ccs811

# from phue import Bridge

GPIO.setmode(GPIO.BCM)

# Temperature and Humidity sensor
sensor = Adafruit_DHT.DHT11
gpio = 16


def fan_on():
    GPIO.output(18, GPIO.HIGH)  # Turn on fan
    print('Too warm!\n Fan ON\n')
    fan_on()


def fan_off():
    GPIO.output(18, GPIO.LOW)  # Turn off fan
    fan_off()


# Air quality
i2c = busio.I2C(board.SCL, board.SDA)
ccs811 = adafruit_ccs811.CCS811(i2c)

while not ccs811.data_ready:
    pass


def light_off():
    GPIO.output(5, GPIO.LOW)  # Turn on Green LED
    print('Light OFF!\n')
    light_off()


def light_on():
    GPIO.output(5, GPIO.HIGH)
    print('Light ON!\n')
    light_on()


#######################################

# Red LED
GPIO.setwarnings(False)
GPIO.setup(21, GPIO.OUT)
GPIO.output(21, GPIO.LOW)

# Yellow LED
GPIO.setup(24, GPIO.OUT)
GPIO.output(24, GPIO.LOW)

# USB Fan
GPIO.setup(18, GPIO.OUT)
GPIO.output(18, GPIO.LOW)

# Green LED
GPIO.setup(5, GPIO.OUT)
GPIO.output(5, GPIO.LOW)

#######################################
# Light sensor
lightsens = 4


def rc_time(lightsens):
    count = 0

    # Output on the pin for
    GPIO.setup(lightsens, GPIO.OUT)
    GPIO.output(lightsens, GPIO.LOW)
    time.sleep(0.1)

    # Change the pin back to input
    GPIO.setup(lightsens, GPIO.IN)

    # Count until the pin goes high
    while GPIO.input(lightsens) == GPIO.LOW:
        count += 1

    return count
